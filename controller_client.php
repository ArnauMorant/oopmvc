<?php
require('model_client.php');

class controller_client extends AbstractController{

    private $conn;

    function __construct($client=0){
        $this->conn=new client();
        switch($client){
            case 0:
                $this->renderTemplate($this->conn->getClients(),file_get_contents('templates/template1.html'));
                break;
            default:
                $this->renderTemplate($this->conn->getSingleClient($client),file_get_contents('templates/template2.html'));
                break;
        }
    }
}  
?>
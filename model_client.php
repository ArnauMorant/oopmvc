<?php
require_once("model_main.php");

class client extends database{

    function __construct(){
        $this->connect();
    }

    function __destruct(){
        $this->db=null;
    }

    function getClients(){
        try{
                $statement = $this->db->prepare('SELECT CLIENTE_COD, NOMBRE, TELEFONO FROM CLIENTE');
                $statement->execute();
                return array('data_table'=>$statement->fetchAll(PDO::FETCH_ASSOC));
            }
            catch(PDOException $e) {
                echo $e->getMessage();
            }
    }
    function getSingleClient($cliente){
        try{
                $statement = $this->db->prepare('SELECT CLIENTE_COD, NOMBRE, TELEFONO FROM CLIENTE WHERE CLIENTE_COD=:cliente');
                $statement->bindParam(':cliente',$cliente);
                $statement->execute();
                return array('data_table'=>$statement->fetchAll(PDO::FETCH_ASSOC));
            }
            catch(PDOException $e) {
                echo $e->getMessage();
            }

    }
}

?>

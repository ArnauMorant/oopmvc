<?php
    
    require('Abstract_Controller.php');

    $action=getAction();

    switch($action[0]){
        case 'borra':
            require_once("controller_delete.php");
            $controller = new controller_delete($action[1]);
            break;
        case 'detalle':
            require_once("controller_client.php");
            $controller = new controller_client($action[1]);
            break;
        default:
            require_once("controller_client.php");
            $controller = new controller_client();
            break;
    } 
     
    function getAction(){
        $uri = $_SERVER['REQUEST_URI'];
        $uri_action=explode('.php',$uri);
        $result_action=explode("/",$uri_action[1]);
        if (sizeOf($result_action)==3) array_shift($result_action);
        return $result_action;
    }
?>

